export const System = {};

System.label = "Chroniques Oubliées Galactiques";
System.name = "co-galactiques";
System.rootPath = "/systems/" + System.name;
System.dataPath = System.rootPath + "/data";
System.templatesPath = System.rootPath + "/templates";
System.debugMode = true;
System.DEV_MODE = true;

System.ASCII = `
   ******    *******     ******
  **////**  **/////**   **////**
 **    //  **     //** **    //
/**       /**      /**/**
/**       /**      /**/**   ****
//**    **//**     ** //**  //**
 //******  //*******   //******
  //////    ///////     ////// `;

export const COG = {};

COG.stats = {
    "str": "COG.stats.str.label",
    "dex": "COG.stats.dex.label",
    "con": "COG.stats.con.label",
    "int": "COG.stats.int.label",
    "wis": "COG.stats.wis.label",
    "cha": "COG.stats.cha.label"
};

COG.skills = {
    "melee": "COG.attacks.melee.label",
    "ranged": "COG.attacks.ranged.label",
    "magic": "COG.attacks.magic.label"
};

COG.statAbbreviations = {
    "str": "COG.stats.str.abbrev",
    "dex": "COG.stats.dex.abbrev",
    "con": "COG.stats.con.abbrev",
    "int": "COG.stats.int.abbrev",
    "wis": "COG.stats.wis.abbrev",
    "cha": "COG.stats.cha.abbrev"
};

COG.itemProperties = {
    "equipable": "COG.properties.equipable",
    "stackable": "COG.properties.stackable",
    "unique": "COG.properties.unique",
    "ranged": "COG.properties.ranged",
    "proficient": "COG.properties.proficient",
    "finesse": "COG.properties.finesse",
    "two-handed": "COG.properties.two-handed",
    "equipment": "COG.properties.equipment",
    "weapon": "COG.properties.weapon",
    "protection": "COG.properties.protection",
    "reloadable": "COG.properties.reloadable",
    "bow": "COG.properties.bow",
    "crossbow": "COG.properties.crossbow",
    "powder": "COG.properties.powder",
    "throwing": "COG.properties.throwing",
    "dr": "COG.properties.dr",
    "sneak": "COG.properties.sneak",
    "powerful": "COG.properties.powerful",
    "critscience": "COG.properties.critscience",
    "specialization": "COG.properties.specialization",
    "effects": "COG.properties.effects",
    "activable": "COG.properties.activable",
    "2H": "COG.properties.2H",
    "13strmin": "COG.properties.13strmin",
    "bashing": "COG.properties.bashing",
    "sling": "COG.properties.sling",
    "spell": "COG.properties.spell",
    "profile": "COG.properties.profile",
    "prestige": "COG.properties.prestige",
    "alternative": "COG.properties.alternative",
    "racial": "COG.properties.racial",
    "creature" : "COG.properties.creature",
    "proneshot" : "COG.properties.proneshot",
    "salve" : "COG.properties.salve",
    "explosive" : "COG.properties.explosive"
};

COG.itemTypes = {
    "profile": "COG.category.profile",
    "capacity": "COG.category.capacity",
    "path": "COG.category.path",
    "trait": "COG.category.trait",
    "item": "COG.category.item",
    "encounterWeapon": "COG.category.encounterWeapon"
};

COG.itemCategories = {
    "armor": "COG.category.armor",
    "shield": "COG.category.shield",
    "melee": "COG.category.melee",
    "ranged": "COG.category.ranged",
    "spell": "COG.category.spell",
    "currency": "COG.category.currency",
    "jewel": "COG.category.jewel",
    "ammunition": "COG.category.ammunition",
    "consumable": "COG.category.consumable",
    "container": "COG.category.container",
    "mount": "COG.category.mount",
    "vehicle": "COG.category.vehicle",
    "trapping": "COG.category.trapping",
    "other": "COG.category.other"
}

COG.itemIcons = {
    "item": "icons/svg/tankard.svg",
    "capacity":"icons/svg/wing.svg",
    "profile": "icons/svg/statue.svg",
    "path": "icons/svg/upgrade.svg",
    "trait": "icons/svg/eye.svg",
    "encounterWeapon": "icons/svg/pawprint.svg"
}

COG.actorIcons = {
    "npc": "icons/svg/angel.svg",
    "encounter": "icons/svg/terror.svg"
}

COG.actorsAllowedItems = {
    "character":[
        "item",
        "capacity",
        "trait",
        "profile",
        "path"
    ],
    "npc":[
        "item",
        "capacity",
        "trait",
        "profile",
        "path"
    ],
    "encounter":[
        "item",
        "capacity",
        "encounterWeapon"
    ]
}

COG.debug = System.debugMode;
